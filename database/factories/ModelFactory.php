<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Models\User;

$factory->define(User::class, function (Faker\Generator $faker) {
    static $password;
    $gender = $faker->randomElement(['male', 'female']);

    return [
        'username'  => $faker->unique()->userName,
        'f_name'    => $faker->firstName($gender),
        'm_name'    => $faker->firstName($gender),
        'l_name'    => $faker->lastName,
        'email'     => $faker->unique()->safeEmail,
        'password'  => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
