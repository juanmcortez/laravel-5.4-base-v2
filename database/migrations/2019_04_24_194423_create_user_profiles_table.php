<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id');

            $table->string('f_name', 128)->nullable();
            $table->string('m_name', 64)->nullable();
            $table->string('l_name', 128)->nullable();

            $table->string('address1', 128)->nullable();
            $table->string('address2', 128)->nullable();
            $table->string('floor', 5)->default(' -- ')->nullable();
            $table->string('apartment', 5)->default(' -- ')->nullable();
            $table->string('city', 64)->nullable();
            $table->string('state', 16)->nullable()->comment('2-5 chars code');
            $table->string('zip', 16)->nullable();
            $table->string('country', 2)->default('US')->nullable()->comment('2 chars code');

            $table->string('homephone', 32)->default('(000) 000-0000')->nullable();
            $table->string('cellphone', 32)->default('(000) 000-0000')->nullable();
            $table->string('facebook', 128)->nullable();
            $table->string('twitter', 128)->nullable();
            $table->string('instagram', 128)->nullable();
            $table->string('snapchat', 128)->nullable();
            $table->string('website', 128)->nullable();

            $table->string('photo', 256)->nullable()->default('storage/defaults/user.jpg');

            $table->string('sex', 6)->default('ban')->nullable()->comment('female - male - ban');

            $table->date('birthday')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
