<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'username', 'email', 'password' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_guard', 'password', 'remember_token',
    ];

    /**
     * Returns the user type
     * @return string
     */
    public function getUserTypeAttribute()
    {
        $usr_type = !empty($this->user_guard) ? $this->user_guard : 0;
        switch ($usr_type) {
            case '99': return 'admin'; break;
            case '9999': return 'superadmin'; break;
            default: case '8': return 'user'; break;
        }
    }

    /**
     * Model relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne('App\Http\Models\UserProfile', 'user_id', 'id');
    }
}
