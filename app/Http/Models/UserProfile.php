<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class UserProfile extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_id', 'f_name', 'm_name', 'l_name',
                            'address1', 'address2', 'floor', 'apartment', 'city', 'state', 'zip', 'country',
                            'facebook', 'twitter', 'instagram', 'snapchat', 'website',
                            'birthday', 'homephone', 'cellphone', 'sex', 'photo' ];

    /**
     * On the fly special attribute
     *
     * @var array
     */
    protected $appends = array('fullname', 'age');

    /**
     * Special attribute for the user profile
     * @return string
     */
    public function getFullNameAttribute()
    {
        $fname = $this->f_name;
        $mname = $this->m_name;
        $lname = $this->l_name;

        // If the user does not contains fname and lname show username
        return !empty($fname) && !empty($lname) ? ($lname.(empty($mname) ? ', '.$fname : ', '.$fname.' '.$mname)) : Auth::user()->username;
    }

    /**
     * Returns the user age based on the birthday
     * @return string
     */
    public function getAgeAttribute()
    {
        // If no birthday, return 0 years.
        return (empty($this->birthday) ? '0' : date_diff(date_create($this->birthday), date_create('now'))->y);
    }

    /**
     * Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'id', 'user_id');
    }
}
