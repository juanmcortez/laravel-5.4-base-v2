<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\UserProfile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserProfileController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming profile request.
     *
     * @param array $profile
     *
     * @return \Illuminate\Validation\Validator
     */
    protected function validator(array $profile)
    {
        return Validator::make($profile, [
            'f_name'    => 'nullable|string|max:128',
            'm_name'    => 'nullable|string|max:64',
            'l_name'    => 'nullable|string|max:128',
            'b_yyyy'    => 'nullable|integer|4',
            'b_mm'      => 'nullable|integer|2',
            'b_dd'      => 'nullable|integer|2',
            'address1'  => 'nullable|string|max:128',
            'address2'  => 'nullable|string|max:128',
            'floor'     => 'nullable|string|max:5',
            'apartment' => 'nullable|string|max:5',
            'city'      => 'nullable|string|max:64',
            'state'     => 'nullable|string|max:16',
            'zip'       => 'nullable|string|max:16',
            'country'   => 'nullable|string|max:2',
            'facebook'  => 'nullable|string|max:128',
            'twitter'   => 'nullable|string|max:128',
            'instagram' => 'nullable|string|max:128',
            'snapchat'  => 'nullable|string|max:128',
            'website'   => 'nullable|string|max:128',
            'homephone' => 'nullable|string|max:32',
            'cellphone' => 'nullable|string|max:32',
            'sex'       => 'nullable|string|max:6',
            'photo'     => 'nullable|string|max:256'
        ]);
    }

    /**
     * Return profile view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('user.profile');
    }


    /**
     * Update the user profile
     *
     * @param Request $profile
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $profile)
    {
        // Update profile model
        $userProfile = UserProfile::firstOrNew(['user_id' => Auth::id()]);
        $userProfile->f_name = $profile['f_name'];
        $userProfile->m_name = $profile['m_name'];
        $userProfile->l_name = $profile['l_name'];

        empty($profile->b_yyyy) ? $birthday = date('Y-') : $birthday = $profile['b_yyyy'] . '-';
        empty($profile->b_mm) ? $birthday .= date('m-') : $birthday .= $profile['b_mm'] . '-';
        empty($profile->b_dd) ? $birthday .= date('d') : $birthday .= $profile['b_dd'];
        $userProfile->birthday = $birthday;

        $userProfile->address1 = $profile['address1'];
        $userProfile->address2 = $profile['address2'];
        $userProfile->floor = $profile['floor'];
        $userProfile->apartment = $profile['apartment'];
        $userProfile->city = $profile['city'];
        $userProfile->state = $profile['state'];
        $userProfile->zip = $profile['zip'];
        $userProfile->country = $profile['country'];

        $userProfile->facebook = $profile['facebook'];
        $userProfile->twitter = $profile['twitter'];
        $userProfile->instagram = $profile['instagram'];
        $userProfile->snapchat = $profile['snapchat'];
        $userProfile->website = $profile['website'];

        $userProfile->homephone = $profile['homephone'];
        $userProfile->cellphone = $profile['cellphone'];

        $userProfile->sex = $profile['sex'];
        $userProfile->photo = $profile['photo'];

        // Save and set notification
        if ($userProfile->save())
        {
            $this->alertStatus('success', '<i class="fas fa-check-circle mr-3"></i>Your profile has been <strong>updated</strong>.');
        } else
        {
            $this->alertStatus('danger', '<i class="fas fa-exclamation-triangle mr-3"></i><strong>Error!!</strong> Something prevented the system from updating your profile.');
        }

        // Return to view
        return redirect(route('profile.show'));
    }


    /**
     * Sets the dynamic message for alerts on the profile screen.
     *
     * @param $status
     * @param $message
     */
    public function alertStatus($status, $message)
    {
        session()->flash('message.level', $status);
        session()->flash('message.content', $message);
    }
}
