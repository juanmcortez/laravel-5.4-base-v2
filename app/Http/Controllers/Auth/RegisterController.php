<?php

namespace App\Http\Controllers\Auth;

use App\Http\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Models\UserProfile;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username'      => 'bail|required|string|max:128|unique:users',
            'email'         => 'required|string|email|max:255|unique:users',
            'password'      => 'required|string|min:6|confirmed',
            'f_name'        => 'nullable|string|max:128',
            'm_name'        => 'nullable|string|max:64',
            'l_name'        => 'nullable|string|max:128',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User|\Illuminate\Database\Eloquent\Model
     */
    protected function create(array $data)
    {
        // Register the new user
        $user = User::create([
            'username'  => $data['username'],
            'email'     => $data['email'],
            'password'  => bcrypt($data['password']),
        ]);

        // Initialize the new user profile
        $profile = UserProfile::create([
            'user_id'   => $user->id,
            'f_name'    => $data['f_name'],
            'm_name'    => $data['m_name'],
            'l_name'    => $data['l_name'],
        ]);

        // Save and set notification
        if($user && $profile){
        $this->alertStatus('success', '<i class="fas fa-check-circle mr-3"></i>Welcome <strong>'.$data['username'].'</strong>.');
        } else {
        $this->alertStatus('danger', '<i class="fas fa-exclamation-triangle mr-3"></i><strong>Error!!</strong> Something prevented the system from creating the user.');
        }

        // Return and login new user
        return $user;
    }


    /**
     * Sets the dynamic message for alerts on the profile screen.
     *
     * @param $status
     * @param $message
     */
    public function alertStatus($status, $message)
    {
        session()->flash('message.level', $status);
        session()->flash('message.content', $message);
    }
}
