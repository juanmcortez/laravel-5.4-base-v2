<?php

/**
 *
 * Set active css class if the specific route name is active.
 *
 * @param string $route_name    A specific route name
 * @param string $class_name    Css class name, optional
 * @return string               Css class name if it's current URI,
 *                              otherwise - empty string
 */
function setActiveRouteItem($route_name, $class_name = 'active')
{
    return Route::currentRouteName() === $route_name ? $class_name : null;
}


/**
 *
 * Set active css class if the specific path is present in the URI. This is for NON-NAMED routes.
 *
 * @param string $path          A specific path element
 * @param string $class_name    Css class name, optional
 * @return string               Css class name if it's current URI,
 *                              otherwise - empty string
 */
function setActiveMenuItem($path, $class_name = 'active')
{
    $request_path = implode('/', Request::segments());

    return $request_path === $path ? $class_name : '';
}