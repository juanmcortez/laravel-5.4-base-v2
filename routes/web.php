<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('landing'); })->name('landing');

Auth::routes();

// The following routes are protected from the user not being logged in
Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/profile', 'UserProfileController@show')->name('profile.show');
    Route::put('/profile', 'UserProfileController@update')->name('profile.update');
});

// Special URL's
Route::get('/404', function () { return view('errors.404'); })->name('404');
Route::get('/500', function () { return view('errors.500'); })->name('500');