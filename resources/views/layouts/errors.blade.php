<!doctype html>
<html lang="{{ app()->getLocale() }}" class="h-100 w-100">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta http-equiv="edit-Type" edit="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') | {{ config('app.name') }}</title>

        <meta name="robots" content="index, follow">
        <meta name="googlebot" content="index, follow">

        <link rel="stylesheet" media="all" href="{{ asset('css/custom.css') }}" />
        <link rel="stylesheet" media="all" href="{{ asset('css/theme.css') }}" />

    </head>
    <body class="d-flex flex-column h-100 w-100">

        <div id="page-content">
            <div class="container">

                @yield('content')

            </div>
        </div>

        <footer id="sticky-footer" class="py-3 bg-dark text-white-50">
            <div class="container text-right small">
                <p class="mb-0">Copyright &copy; Your Website</p>
            </div>
        </footer>

        <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/theme.js') }}" type="text/javascript"></script>
    </body>
</html>