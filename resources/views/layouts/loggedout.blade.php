<!doctype html>
<html lang="{{ app()->getLocale() }}" class="h-100 w-100">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta http-equiv="edit-Type" edit="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') | {{ config('app.name') }}</title>

        <meta name="robots" content="index, follow">
        <meta name="googlebot" content="index, follow">

        <link rel="stylesheet" media="all" href="{{ asset('css/custom.css') }}" />
        <link rel="stylesheet" media="all" href="{{ asset('css/theme.css') }}" />

    </head>
    <body class="d-flex flex-column h-100 w-100">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow-sm">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ route('landing') }}">
                    {{ config('app.name') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item {{ setActiveRouteItem('landing') }}">
                            <a class="nav-link" href="{{ route('landing') }}">
                                <span class="sr-only">Home</span>
                                Home
                            </a>
                        </li>
                        <li class="nav-item {{ setActiveRouteItem('login') }}">
                            <a class="nav-link" href="{{ route('login') }}">
                                <span class="sr-only">Sign In</span>
                                Sign In
                            </a>
                        </li>
                        <li class="nav-item {{ setActiveRouteItem('register') }}">
                            <a class="nav-link" href="{{ route('register') }}">
                                <span class="sr-only">Register</span>
                                Register
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-content">
            <div class="container">

                @yield('content')

            </div>
        </div>

        <footer id="sticky-footer" class="py-3 bg-dark text-white-50">
            <div class="container text-right small">
                <p class="mb-0">Copyright &copy; Your Website</p>
            </div>
        </footer>

        <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/theme.js') }}" type="text/javascript"></script>
    </body>
</html>