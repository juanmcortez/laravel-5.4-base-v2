<!doctype html>
<html lang="{{ app()->getLocale() }}" class="h-100 w-100">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta http-equiv="edit-Type" edit="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') | {{ config('app.name') }}</title>

        <meta name="robots" content="index, follow">
        <meta name="googlebot" content="index, follow">

        <link rel="stylesheet" media="all" href="{{ asset('css/custom.css') }}" />
        <link rel="stylesheet" media="all" href="{{ asset('css/theme.css') }}" />

    </head>
    <body class="d-flex flex-column h-100 w-100">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow-sm">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ route('home') }}">
                    {{ config('app.name') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                @if(!Auth::guest())
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown user-dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user-circle"></i> Welcome <strong>{{ Auth::user()->profile->fullname }}</strong> !! <span class="caret"></span>
                            </a>
                            <!-- Here's the magic. Add the .animate and .slide-in classes to your .dropdown-menu and you're all set! -->
                            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('profile.show') }}">
                                    <i class="fas fa-user-cog mr-2"></i> Profile
                                </a>
                                <div class="dropdown-divider"></div>
                                <div class="dropdown-item-text text-black-50">
                                    <i title="Type" data-title="Type" class="fas fa-user-shield mr-2"></i> {{ ucfirst(Auth::user()->usertype) }}
                                    <i title="Age" data-title="Age" class="fas fa-birthday-cake ml-5 mr-2"></i> {{ Auth::user()->profile->age }} yrs.
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="dropdown-item-text text-black-50" title="User's E-mail address" data-title="User's E-mail address">
                                    <i class="fas fa-at mr-2"></i> {{ Auth::user()->email }}
                                </div>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt mr-2"></i> Sign Out
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                            </div>
                        </li>
                        @if(Auth::user()->user_guard >= 9999)
                        <li class="nav-item dropdown ml-3">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                App <span class="caret"></span>
                            </a>
                            <!-- Here's the magic. Add the .animate and .slide-in classes to your .dropdown-menu and you're all set! -->
                            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
                                <div class="dropdown-item-text text-black-50">
                                    <i class="fas fa-cogs"></i> Options
                                </div>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div>
                @endif

            </div>
        </nav>

        <div id="page-content">
            <div class="container">

                @yield('content')

            </div>
        </div>

        <footer id="sticky-footer" class="py-3 bg-dark text-white-50">
            <div class="container text-right small">
                <p class="mb-0">Copyright &copy; Your Website</p>
            </div>
        </footer>

        <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/theme.js') }}" type="text/javascript"></script>
    </body>
</html>