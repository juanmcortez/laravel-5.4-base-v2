<?php
/**
 * Filename: 500.blade.php
 * Created by Juan M. Cortéz on 03-05-2019
 *
 * Project: laravel5.4_base_v2
 * Copyright: All rights reserved 2009 - 2019 © Juan M. Cortéz
 * License: MIT
 */
@extends('layouts.errors')

@section('title', '500')

@section('content')

    <div class="row justify-content-center text-center">
        <div class="col-md-12">
            <h1 class="font-weight-bolder mt-4">500</h1>
            <p>
                <a class="btn btn-link" href="javascript:history.back()">
                    <i class="fas fa-arrow-left"></i> Go back to previous page
                </a>
            </p>
        </div>
    </div>

@endsection