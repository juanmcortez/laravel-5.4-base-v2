@extends('layouts.errors')

@section('title', '404')

@section('content')

    <div class="row justify-content-center text-center">
        <div class="col-md-12">
            <h1 class="font-weight-bolder mt-4">404</h1>
            <p>
                <a class="btn btn-link" href="javascript:history.back()">
                    <i class="fas fa-arrow-left"></i> Go back to previous page
                </a>
            </p>
        </div>
    </div>

@endsection