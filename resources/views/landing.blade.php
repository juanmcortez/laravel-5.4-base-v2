@extends('layouts.loggedout')

@section('title', 'Welcome!')

@section('content')

                <div class="row justify-content-center text-center">
                    <div class="col-md-12">
                        <h1 class="font-weight-light mt-4 text-white">Sticky Footer using Flexbox</h1>
                        <p class="lead text-white-50">Use just two Bootstrap 4 utility classes and three custom CSS rules and you will have a flexbox enabled sticky footer for your website!</p>
                    </div>
                </div>

@endsection