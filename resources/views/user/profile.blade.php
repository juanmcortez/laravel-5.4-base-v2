@extends('layouts.loggedin')

@section('title', Auth::user()->profile->fullname.' Profile')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="font-weight-bold mt-4 text-white">{{ Auth::user()->profile->fullname }} Profile</h1>
            <hr />
        </div>
    </div>

    @if(session()->has('message.level'))
    <div class="alert alert-{{ session('message.level') }} alert-dismissible fade show" role="alert">
        {!! session('message.content') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <form action="{{ route('profile.update') }}" method="POST">

        {{ csrf_field() }}

        {{ method_field('PUT') }}

        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="username" class="col-form-label">Username</label>
            </div>
            <div class="col-md-6">
                <input name="username" id="username" type="text" class="form-control" disabled value="{{ Auth::user()->username }}" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="email" class="col-form-label">E-mail address</label>
            </div>
            <div class="col-md-6">
                <input name="email" id="email" type="email" class="form-control" value="{{ Auth::user()->email }}" disabled />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label class="col-form-label">Full Name</label>
            </div>
            <div class="col-md-2">
                <input name="f_name" id="f_name" type="text" class="form-control" autofocus value="{{ Auth::user()->profile->f_name }}" placeholder="First Name" />
            </div>
            <div class="col-md-2">
                <input name="m_name" id="m_name" type="text" class="form-control" value="{{ Auth::user()->profile->m_name }}" placeholder="Middle Name" />
            </div>
            <div class="col-md-2">
                <input name="l_name" id="l_name" type="text" class="form-control" value="{{ Auth::user()->profile->l_name }}" placeholder="Last Name" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label class="col-form-label">Birthday</label>
            </div>
            @php
            $bYear = !empty(Auth::user()->profile->birthday) ? substr(Auth::user()->profile->birthday,0,4) : '';
            $bMont = !empty(Auth::user()->profile->birthday) ? substr(Auth::user()->profile->birthday,5,2) : '';
            $bDays = !empty(Auth::user()->profile->birthday) ? substr(Auth::user()->profile->birthday,8,2) : '';
            @endphp
            <div class="col-md-1">
                <input name="b_yyyy" id="b_yyyy" type="text" class="form-control text-center" value="{{ $bYear }}" placeholder="YYYY" maxlength="4" />
            </div>
            <div class="col-md-1">
                <input name="b_mm" id="b_mm" type="text" class="form-control text-center" value="{{ $bMont }}" placeholder="MM" maxlength="2" />
            </div>
            <div class="col-md-1">
                <input name="b_dd" id="b_dd" type="text" class="form-control text-center" value="{{ $bDays }}" placeholder="DD" maxlength="2" />
            </div>
            <div class="col-md-3 text-center">
                <label class="col-form-label">Age: {{ Auth::user()->profile->age }} yrs.</label>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="address1" class="col-form-label">Address</label>
            </div>
            <div class="col-md-6">
                <input name="address1" id="address1" type="text" class="form-control" value="{{ Auth::user()->profile->address1 }}" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="address2" class="col-form-label">&nbsp;</label>
            </div>
            <div class="col-md-6">
                <input name="address2" id="address2" type="text" class="form-control" value="{{ Auth::user()->profile->address2 }}" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="floor" class="col-form-label">Floor</label>
            </div>
            <div class="col-md-2">
                <input name="floor" id="floor" type="text" class="form-control" value="{{ Auth::user()->profile->floor }}" />
            </div>
            <div class="col-md-2 text-right">
                <label for="apartment" class="col-form-label">Apartment</label>
            </div>
            <div class="col-md-2">
                <input name="apartment" id="apartment" type="text" class="form-control" value="{{ Auth::user()->profile->apartment }}" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="city" class="col-form-label">City</label>
            </div>
            <div class="col-md-2">
                <input name="city" id="city" type="text" class="form-control" value="{{ Auth::user()->profile->city }}" />
            </div>
            <div class="col-md-2 text-right">
                <label for="state" class="col-form-label">State</label>
            </div>
            <div class="col-md-2">
                <input name="state" id="state" type="text" class="form-control" value="{{ Auth::user()->profile->state }}" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="zip" class="col-form-label">Zip</label>
            </div>
            <div class="col-md-2">
                <input name="zip" id="zip" type="text" class="form-control" value="{{ Auth::user()->profile->zip }}" />
            </div>
            <div class="col-md-2 text-right">
                <label for="country" class="col-form-label">Country</label>
            </div>
            <div class="col-md-2">
                <input name="country" id="country" type="text" class="form-control" value="{{ Auth::user()->profile->country }}" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="homephone" class="col-form-label">Phone</label>
            </div>
            <div class="col-md-2">
                <input name="homephone" id="homephone" type="text" class="form-control" value="{{ Auth::user()->profile->homephone }}" />
            </div>
            <div class="col-md-2 text-right">
                <label for="cellphone" class="col-form-label">Cellphone</label>
            </div>
            <div class="col-md-2">
                <input name="cellphone" id="cellphone" type="text" class="form-control" value="{{ Auth::user()->profile->cellphone }}" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="sex" class="col-form-label">Gender</label>
            </div>
            <div class="col-md-2">
                <input name="sex" id="sex" type="text" class="form-control" value="{{ Auth::user()->profile->sex }}" />
            </div>
            <div class="col-md-2 text-right">
                <label for="photo" class="col-form-label">Photo</label>
            </div>
            <div class="col-md-2">
                <input name="photo" id="photo" type="text" class="form-control" value="{{ Auth::user()->profile->photo }}" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="facebook" class="col-form-label">
                    <i class="fab fa-facebook-f"></i>
                </label>
            </div>
            <div class="col-md-2">
                <input name="facebook" id="facebook" type="text" class="form-control" value="{{ Auth::user()->profile->facebook }}" />
            </div>
            <div class="col-md-2 text-right">
                <label for="twitter" class="col-form-label">
                    <i class="fab fa-twitter"></i>
                </label>
            </div>
            <div class="col-md-2">
                <input name="twitter" id="twitter" type="text" class="form-control" value="{{ Auth::user()->profile->twitter }}" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="instagram" class="col-form-label">
                    <i class="fab fa-instagram"></i>
                </label>
            </div>
            <div class="col-md-2">
                <input name="instagram" id="instagram" type="text" class="form-control" value="{{ Auth::user()->profile->instagram }}" />
            </div>
            <div class="col-md-2 text-right">
                <label for="snapchat" class="col-form-label">
                    <i class="fab fa-snapchat-square"></i>
                </label>
            </div>
            <div class="col-md-2">
                <input name="snapchat" id="snapchat" type="text" class="form-control" value="{{ Auth::user()->profile->snapchat }}" />
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-2 text-right">
                <label for="website" class="col-form-label">
                    <i class="fab fa-internet-explorer"></i>
                </label>
            </div>
            <div class="col-md-6">
                <input name="website" id="website" type="text" class="form-control" value="{{ Auth::user()->profile->website }}" />
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">&nbsp;</div>
            <div class="col-md-2">
                <button class="btn btn-success btn-block" type="submit">
                    <i class="fas fa-pencil-alt"></i> Update
                </button>
            </div>
        </div>
    </form>
@endsection