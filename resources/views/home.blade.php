@extends('layouts.loggedin')

@section('title', 'Home')

@section('content')
                @if (session('status'))
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    </div>
                </div>
                @endif
                <div class="row justify-content-center text-center">
                    <div class="col-md-12">
                        <h1 class="font-weight-light mt-4 text-white">Home</h1>
                        <p class="lead text-white-50">You are logged in!</p>
                    </div>
                </div>
@endsection
