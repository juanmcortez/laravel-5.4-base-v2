@extends('layouts.loggedout')

@section('title', 'Welcome!')

@section('content')
                <div class="row no-gutter flex-row">
                    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image h-100">
                        {{-- SIDE IMAGE HOLDER --}}
                    </div>
                    <div class="col-md-8 col-lg-6 h-100">
                        <div class="register d-flex align-items-center py-5">
                            {{-- FORM HOLDER --}}
                            <div class="card w-100">
                                <div class="card-header">
                                    Register!
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">

                                        {{ csrf_field() }}

                                        <div class="row form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                            <label for="username" class="col-md-4 col-form-label text-md-right">Username</label>
                                            <input id="username" type="text" class="form-control col-md-7" name="username" value="{{ old('username') }}" required autofocus>
                                            @if ($errors->has('username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="row form-group{{ $errors->has('f_name') || $errors->has('l_name') ? ' has-error' : '' }}">
                                            <label class="col-md-4 col-form-label text-md-right">Full Name</label>
                                            <input id="firstName" type="text" class="form-control col-md-3"  placeholder="First Name" name="f_name" value="{{ old('f_name') }}" />
                                            @if ($errors->has('f_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('f_name') }}</strong>
                                                </span>
                                            @endif
                                            <input id="middleName" type="text" class="form-control col-md-1"  placeholder="Middle" name="m_name" value="{{ old('m_name') }}" />
                                            <input id="lastName" type="text" class="form-control col-md-3"  placeholder="Last Name" name="l_name" value="{{ old('l_name') }}" />
                                            @if ($errors->has('l_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('l_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                            <input id="email" type="email" class="form-control col-md-7" name="email" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                            <input id="password" type="password" class="form-control col-md-7" name="password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="row form-group">
                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm password</label>
                                            <input id="password-confirm" type="password" class="form-control col-md-7" name="password_confirmation" required>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-7 offset-md-4">
                                                <button type="submit" class="btn btn-primary btn-block text-uppercase font-weight-bold">
                                                    Register
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
