@extends('layouts.loggedout')

@section('title', 'Reset your password')

@section('content')
                <div class="row no-gutter flex-row">
                    <div class="col-md-8 offset-md-2 h-100">
                        <div class="login d-flex align-items-center py-5">
                            {{-- FORM HOLDER --}}
                            <div class="card w-100">
                                <div class="card-header">
                                    Reset Password
                                </div>
                                <div class="card-body">

                                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">

                                        {{ csrf_field() }}

                                        <input type="hidden" name="token" value="{{ $token }}">

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                            <input id="email" type="email" class="form-control col-md-6" name="email" value="{{ $email or old('email') }}" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                            <input id="password" type="password" class="form-control col-md-6" name="password" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} row">
                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                                            <input id="password-confirm" type="password" class="form-control col-md-6" name="password_confirmation" required>

                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary btn-block text-uppercase font-weight-bold">
                                                    Reset Password
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
