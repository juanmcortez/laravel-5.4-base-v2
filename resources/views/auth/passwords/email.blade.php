@extends('layouts.loggedout')

@section('title', 'Reset your password')

@section('content')
                <div class="row no-gutter flex-row">
                    <div class="col-md-8 offset-md-2 h-100">
                        <div class="login d-flex align-items-center py-5">
                            {{-- FORM HOLDER --}}
                            <div class="card w-100">
                                <div class="card-header">
                                    Reset Password
                                </div>
                                <div class="card-body">

                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">

                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                                            <label for="email" class="col-md-3 col-form-label text-md-right">E-Mail Address</label>
                                            <input id="email" type="email" class="form-control col-md-4 mr-3" name="email" value="{{ old('email') }}" required autofocus>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                            <button type="submit" class="btn btn-primary col-md-4">
                                                Send Password Reset
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
