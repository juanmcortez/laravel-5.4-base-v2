@extends('layouts.loggedout')

@section('title', 'Welcome back!')

@section('content')
                <div class="row no-gutter flex-row">
                    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image h-100">
                        {{-- SIDE IMAGE HOLDER --}}
                    </div>
                    <div class="col-md-8 col-lg-6 h-100">
                        <div class="login d-flex align-items-center py-5">
                            {{-- FORM HOLDER --}}
                            <div class="card w-100">
                                <div class="card-header">
                                    Welcome back!
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">

                                        {{ csrf_field() }}

                                        <div class="row form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                            <label for="username" class="col-md-4 col-form-label text-md-right">Username</label>
                                            <input id="username" type="text" class="form-control col-md-7" name="username" value="{{ old('username') }}" required autofocus>
                                            @if ($errors->has('username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                            <input id="password" type="password" class="form-control col-md-7" name="password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-7 offset-md-4">
                                                <div class="checkbox">
                                                    <label class="w-100 text-center">
                                                        <input type="checkbox" class="form-check-inline" name="remember" {{ old('remember') ? 'checked' : '' }} /> Remember Me
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-7 offset-md-4">
                                                <button type="submit" class="btn btn-primary btn-block text-uppercase font-weight-bold mb-2">
                                                    Sign in
                                                </button>
                                                <a class="btn btn-link small w-100 text-center" href="{{ route('password.request') }}">
                                                    Forgot Your Password?
                                                </a>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
