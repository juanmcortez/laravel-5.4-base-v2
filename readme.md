# Basic Project

This is the base for developing a project on Laravel 5.4 and Vue.

<a href="https://gitlab.com/juanmcortez/laravel5.4_base_v2">
    <img src="https://img.shields.io/badge/version-v1.0.0-orange.svg" alt="v1.0.0" />
</a>

## Installation

Once you have cloned the project, follow this steps:

- Step 1 
```bash
$ composer install
```
- Step 2
```bash
$ sudo chown -R www-data:www-data project-folder/
``` 
- Step 3
```bash
$ sudo chmod -R 0755 project-folder/
```
- Step 4
```bash
$ sudo chmod -R 0775 project-folder/bootstrap
```
- Step 5
```bash
$ sudo chmod -R 0775 project-folder/storage
```
- Step 6: copy .env.example and edit the file
```bash
$ cp .env.example .env
```
- Step 7
```bash
$ php artisan config:clear
```
- Step 8
```bash
$ php artisan key:generate
```
- Step 9
```bash
$ npm install
```
- Step 10
```bash
$ npm run dev
```
- Step 11: Enable "Laravel plugin" in phpstorm if needed.

**That's it!** Start coding!! And **_don't forget to delete the git folder_** and init a new one!!

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[PROJECT](https://gitlab.com/juanmcortez/laravel5.4_base_v2/raw/master/LICENSE)

[MIT](https://choosealicense.com/licenses/mit/)